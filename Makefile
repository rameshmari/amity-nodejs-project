# Docker registry
REGISTRY ?= index.docker.io
#Image namespace
NAMESPACE ?= rameshmari156
# image name
NAME ?= demo-nodejs-mongodb
#image default tag
IMAGE_TAG ?= latest

IMAGE_NAME = ${NAMESPACE}/${NAME}:${IMAGE_TAG}

build:
	docker build --build-arg ConnectionString="${ConnectionString}" -t ${IMAGE_NAME} .

push:
	docker push ${IMAGE_NAME}

clean_image:
	docker rmi ${IMAGE_NAME}

k8s_deploy:
	kubectl apply -f k8s

k8s_clean:
	kubectl delete -f k8s
