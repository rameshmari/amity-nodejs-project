FROM rameshmari156/demo-nodejs-mongodb:latest 

ARG ConnectionString="mongodb+srv://jack:<password>@cluster0.cfnj9.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

ENV MONGODB_ADDON_URI=$ConnectionString

COPY . .

RUN npm install

ENTRYPOINT npm start
